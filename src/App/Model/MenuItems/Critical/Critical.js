import React, {useState, Fragment} from "react";
import "./test.css";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Grid from '@mui/material/Grid';
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import {backend} from "../../../../Config/backend";
import {useAlert} from "react-alert";


/**
 *
 * @returns {JSX.Element}
 * @constructor
 */
export default function Critical() {
    const [inputValue, setInputValue] = useState({name: "", price: ""});
    const {name, price} = inputValue;
    const alert = useAlert();
    const handleChange = (e) => {
        const {name, value} = e.target;
        setInputValue((prev) => ({
            ...prev,
            [name]: value,
        }));
    };
    const handleSubmit = (e) => {
        const {name, value} = e.target;
        setInputValue((prev) => ({
            ...prev,
            [name]: value,
        }));
        fetch(backend.critical, {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json;x-www-form-urlencoded',
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer'
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            if (data.status === 'success') {
                return (
                    <div>
                        {alert.show(data.data)}
                    </div>
                );
            }
        });
    };
    return (
        <Fragment>
            <Container maxWidth="lg=12" className="App">
                <Box sx={{bgcolor: '#cfe8fc', height: '100vh'}}>
                    <Grid container spacing={2}>
                        <Grid item xs={3}>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                name="name"
                                label="Название заклинания"
                                type="input"
                                fullWidth
                                variant="standard"
                                onChange={handleChange}
                            />
                        </Grid>
                        <Grid item xs={3}>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                name="price"
                                label="Название заклинания"
                                type="input"
                                fullWidth
                                variant="standard"
                                onChange={handleChange}
                            />
                        </Grid>
                        <Grid item xs={3}>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                name="price"
                                label="Название заклинания"
                                type="input"
                                fullWidth
                                variant="standard"
                                onChange={handleChange}
                            />
                        </Grid>
                        <Grid item xs={3}>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                name="price"
                                label="Название заклинания"
                                type="input"
                                fullWidth
                                variant="standard"
                                onChange={handleChange}
                            />
                        </Grid>
                    </Grid>
                    <Button onClick={handleSubmit}>Расчитать</Button>
                </Box>
            </Container>
        </Fragment>
    )

}