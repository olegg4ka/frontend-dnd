import React from 'react';
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import "./spells.css";
import RegulationDialogSpell from "./RegulationDialogSpell";
import { DataGrid, GridRowsProp, GridColDef } from '@mui/x-data-grid';


const rows: GridRowsProp =  JSON.parse(localStorage.getItem('Spells'));


const columns: GridColDef[] = [
    // { field: 'id', headerName: 'id', width: 150 },
    { field: 'name', headerName: 'Название', width: 150, },
    { field: 'lvl', headerName: 'Уровень', width: 100, align: "center" },
    { field: 'magicSchool', headerName: 'Школа магии', width: 120, align: "center" },
    { field: 'components', headerName: 'Компоненты', width: 150 },
    { field: 'distance', headerName: 'Дистаниция', width: 150, align: "center" },
    { field: 'duration', headerName: 'Длительность', width: 260 },
    { field: 'description', headerName: 'Описание', width: 600 },
];

/**
 * Заклинания
 * @returns {JSX.Element}
 * @constructor
 */
export default function Spells() {
    return (
        <Container maxWidth="lg=12" className="App">
            <Box sx={{bgcolor: '#cfe8fc', height: '100vh'}}>
                <h2>Заклинания</h2>
                <Box sx={{bgcolor: '#cfe8fc', height: 'auto', marginLeft: 2, marginTop: 8}}>
                    <div style={{ height: '100%', width: '99%' }}>
                        <DataGrid rows={rows} columns={columns} />
                    </div>
                </Box>
                <RegulationDialogSpell/>
            </Box>
        </Container>
    );
}


