import * as React from 'react';
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import TextField from "@mui/material/TextField";
import DialogActions from "@mui/material/DialogActions";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import request from "../../../../Сore/Request/Request";

/**
 * Создание заклинания
 */
export default class CreateSpell extends React.Component {
    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            magicSchool: '',
            description: '',
            lvl: '',
            distance: '',
            components: '',
            duration: '',
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    /**
     *
     * @param event
     */
    handleChange(event) {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    /**
     *
     * @param event
     */
    handleSubmit(event) {
        request('http://dnd.v2.local/api/createSpell', {
            name:  this.state.name,
            magicSchool:  this.state.magicSchool,
            description:  this.state.description,
            lvl:  this.state.lvl,
            distance:  this.state.distance,
            components:  this.state.components,
            duration:  this.state.duration,
        });
        this.props.handleCloseDialog();
        event.preventDefault();
    }

    /**
     *
     * @returns {(function(): *)|*}
     */
    render() {
        return (
            <Dialog open={this.props.openDialog} onClose={this.props.handleClose}>
                <DialogTitle>Создайте свое заклинание!!</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Создайте свое заклинание!!
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Название заклинания"
                        type="input"
                        fullWidth
                        variant="standard"
                        onChange={this.handleChange}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        id="magicSchool"
                        label="Школа заклинания"
                        type="input"
                        fullWidth
                        variant="standard"
                        onChange={this.handleChange}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        id="description"
                        label="Описание заклинания"
                        type="input"
                        fullWidth
                        variant="standard"
                        onChange={this.handleChange}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        id="lvl"
                        label="Уровень заклинания"
                        type="input"
                        fullWidth
                        variant="standard"
                        onChange={this.handleChange}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        id="distance"
                        label="Дистанция"
                        type="input"
                        fullWidth
                        variant="standard"
                        onChange={this.handleChange}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        id="components"
                        label="Компоненты"
                        type="input"
                        fullWidth
                        variant="standard"
                        onChange={this.handleChange}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        id="duration"
                        label="Длительность"
                        type="input"
                        fullWidth
                        variant="standard"
                        onChange={this.handleChange}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.props.handleCloseDialog}>Закрыть</Button>
                    <Button type="submit" onClick={this.handleSubmit}>Сохранить</Button>
                </DialogActions>
            </Dialog>
        );
    }
}

