import {backend} from "../../../Config/backend";

/**
 *  Старт приложения
 */
export default async function startApp(url = backend.start, params = null) {
    let arr = ['Spells', 'Class', 'Monsters', 'Race', 'Roles', 'Skills'];
    for (let i = 0; i < arr.length; i++){
        if (!localStorage.getItem(arr[i])) {
            try {
                const response = await fetch(url, {
                    method: 'POST',
                    mode: 'cors',
                    cache: 'no-cache',
                    credentials: 'same-origin',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                    },
                    redirect: 'follow',
                    referrerPolicy: 'no-referrer',
                    body: JSON.stringify(params)
                });
                if (!response.ok && response.status !== 200) {
                    throw new Error('Ошибка запроса');
                }
                let json = await response.json();
                localStorage.setItem('Spells', JSON.stringify(json.data.spells));
                localStorage.setItem('Class', JSON.stringify(json.data.class));
                localStorage.setItem('Monsters', JSON.stringify(json.data.monsters));
                localStorage.setItem('Race', JSON.stringify(json.data.race));
                localStorage.setItem('Roles', JSON.stringify(json.data.roles));
                localStorage.setItem('Skills', JSON.stringify(json.data.skills));
            } catch (error) {
                console.error(error);
            }
        }
    }

}