import qs from 'qs';
import {backend} from "../../Config/backend";

const axios = require('axios');

axios.get('/user?ID=12345')
    .then(function (response) {
        console.log(response);
    })
    .catch(function (error) {
        console.log(error);
    })
    .then(function () {
    });

axios.get('/user', {
    params: {
        ID: 12345
    }
})
    .then(function (response) {
        console.log(response);
    })
    .catch(function (error) {
        console.log(error);
    })
    .then(function () {
    });

export async function getUser() {
    try {
        const response = await axios.get(backend.url);
        console.log(response);
    } catch (error) {
        console.error(error);
    }
}