import {backend} from "../../Config/backend";

/**
 *
 * @param url
 * @param params
 * @returns {Promise<string>}
 */
export default async function request(url = backend.url, params = "") {
    try {
        const response = await fetch(url, {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify(params)
        });
        if (!response.ok) {
            throw new Error('Ошибка запроса');
        }
        return await response.json();
    } catch (error) {
        console.error(error);
        return JSON.stringify({'status':'error'});
    }
}