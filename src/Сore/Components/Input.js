const {useState} = require("react");

export default function Input(inputInfo = {}) {
    const [value, setValue] = useState('')

    return (
        <div>
            <input
                value={value}
                onChange={e => setValue(value)}
            />
        </div>
    )
}