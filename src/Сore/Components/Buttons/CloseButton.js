import React from "react";
import Button from "@mui/material/Button";
import Input from "../Input/Input";

/**
 *
 */
export default class SubmitButton extends React.Component {

    /**
     *
     * @param props
     * @param title
     * @param type
     */
    constructor(props, title, type) {
        super(props);
        this.title = title;
        this.type = type;
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    /**
     *
     * @param event
     */
    handleSubmit(event) {
        console.log(this);
        event.preventDefault();
    }

    /**
     *
     * @returns {JSX.Element}
     */
    render() {
        return (
            <Button type={this.props.type} onClick={this.handleSubmit}>{this.props.title}</Button>
        );
    }

}