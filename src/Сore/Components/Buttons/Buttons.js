import * as React from 'react';
import Button from '@mui/material/Button';

export function close(event){
    return (
        <Button onClick={event}>Закрыть</Button>
    );
}

export function save(event){
    return (
        <Button onClick={event}>Сохранить</Button>
    );
}
