import React from 'react';

/**
 * Время
 */
class Time extends React.Component {
    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.time = {date: new Date()};
    }

    /**
     *
     * @returns {JSX.Element}
     */
    render() {
        return (
            <div className="Time">
                <h2>{this.time.date.toLocaleTimeString()}.</h2>
            </div>
        );
    }

}

/**
 *
 * @returns {JSX.Element}
 * @constructor
 */
export default new Time().render();