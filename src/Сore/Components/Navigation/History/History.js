import React, {Component} from 'react'
import {createBrowserHistory} from 'history'
import {
    Route,
    Switch,
    Redirect,
    withRouter
} from "react-router-dom"
const history = createBrowserHistory()

class History extends Component {
    render() {
        const { history } = this.props

        return (
            <div className="App">
                <Switch>
                    <Route history={history} path='/home' component={Home} />
                    <Route history={history} path='/appointments' component={Appointments} />
                    <Redirect from='/' to='/home'/>
                </Switch>
            </div>
        );
    }
}

export default withRouter(History)
