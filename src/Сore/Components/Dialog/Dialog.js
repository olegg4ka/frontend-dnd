import * as React from 'react';
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import TextField from "@mui/material/TextField";
import DialogActions from "@mui/material/DialogActions";
import {close, save} from "../Buttons/Buttons";
import Dialog from "@mui/material/Dialog";

/**
 *
 * @returns {JSX.Element}
 */
export default function CustomSaveDialog() {

    const [open, setOpen] = React.useState(false);

    const handleOpenDialog = () => {
        setOpen(true);
    };

    const handleCloseDialog = () => {
        setOpen(false);
    };

    const handleSaveSpell = () => {
        // request('http://dnd.v2.local/api/user', {skill: 'FIREBALL'});
        setOpen(false);
    };

    return (

        <Dialog open={open} onClose={handleOpenDialog}>
            <DialogTitle>Создайте свое заклинание!!</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Создайте свое заклинание!!
                </DialogContentText>
                <TextField
                    autoFocus
                    margin="dense"
                    id="name"
                    label="Название заклинания"
                    type="email"
                    fullWidth
                    variant="standard"

                />
            </DialogContent>
            <DialogActions>
                {close(handleCloseDialog)}
                {save(handleSaveSpell)}
            </DialogActions>
        </Dialog>

    );
}