import React from 'react';
import TextField from "@mui/material/TextField";
import { styled } from '@mui/material/styles';

/**
 * Input
 */
export default class Input extends React.Component {
    /**
     *
     * @param props
     * @param id
     * @param type
     * @param label
     * @param required
     */
    constructor(props, id, type, label, required = false) {
        super(props);
        this.id = id;
        this.required = required
        this.label = label;
        this.type = type;
        this.state = {id: '4'};
        this.handleChange = this.handleChange.bind(this);
    }

    /**
     *
     * @param event
     */
    handleChange(event) {
        this.setState({value: event.target.value});
    }

    /**
     *
     * @returns {JSX.Element}
     */
    render() {
        let required = this.props.required;
        if (!this.props.required){
            required = this.required;
        }
        return (
            <label>
                {this.label}
                <ValidationTextField
                    required = {required}
                    variant="outlined"
                    margin = "dense"
                    id = {this.props.id}
                    label = {this.props.label}
                    type = {this.props.type}
                    fullWidth
                    size = "normal"
                    onChange = {this.handleChange}
                />
            </label>
        );
    }
}

/**
 *
 * @type {StyledComponent<PropsOf<(props: TextFieldProps) => JSX.Element> & MUIStyledCommonProps<Theme>, {}, {}>}
 */
const ValidationTextField = styled(TextField)({
    "& input:valid + fieldset": {
        borderColor: "gray",
        borderWidth: 2
    },
    "& input:invalid + fieldset": {
        borderColor: "red",
        borderWidth: 2
    },
    "& input:valid:focus + fieldset": {
        borderLeftWidth: 6,
        padding: "4px !important" // override inline-style
    }
});
