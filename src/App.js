import './App.css';
import * as React from 'react';
import {BrowserRouter as Router, Link, Route, Switch} from "react-router-dom";
import router from "./Config/Routes";
import SideBar from "./Сore/Components/SideBar/SideBar";
import CssBaseline from '@mui/material/CssBaseline';
import StartApp from "./App/Model/StartApp/StartApp";
import AlertTemplate from "react-alert-template-basic";
import {positions, types, Provider} from "react-alert";

const options = {
    timeout: 5000,
    position: positions.MIDDLE,
    types: types.INFO
};

/**
 *
 * @returns {JSX.Element}
 * @constructor
 */
function App() {
    StartApp();
    React.useEffect(() => {
        const menuWrap = document.querySelector(".bm-menu-wrap");
        if (menuWrap) {
            menuWrap.setAttribute("aria-hidden", true);
        }
    }, []);
    return (
        <Provider template={AlertTemplate} {...options}>
            <Router>
                <React.Fragment>
                    <CssBaseline/>
                    <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"}/>
                </React.Fragment>
                <Switch>
                    {router.map((item) => {
                        return <Route exact path={item['route']}>
                            {item['switch']}
                        </Route>;
                    })}
                </Switch>
            </Router>
        </Provider>
    )
        ;
}


export default App;
