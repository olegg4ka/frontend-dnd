import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {StyledEngineProvider} from '@mui/material/styles';

const rootElement = document.getElementById("root");

ReactDOM.render(
    <React.StrictMode>
        <StyledEngineProvider injectFirst>
            <App/>
        </StyledEngineProvider>
    </React.StrictMode>, rootElement
);

reportWebVitals();
